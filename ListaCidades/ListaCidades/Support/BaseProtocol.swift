//
//  BaseProtocol.swift
//  ListaCidades
//
//  Created by Saulo Costa on 29/07/17.
//  Copyright © 2017 Saulo. All rights reserved.
//

import Foundation

protocol BaseProtocol {
    func showLoader()
    func hideLoader()
    func showLoader(withStatus status: String)
    func showError(withStatus status: String)
    func showSuccess(withStatus status: String)
    func showInfo(withStatus status: String)
}
