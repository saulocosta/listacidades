
import UIKit
import Gloss

enum RequestMethod: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
    case delete = "DELETE"
}

// MARK: - Response
struct RequestResponse {
    var responseItem: Any?
    var success = false
    var message = ""
    
    init(withResponse response: Data?) {
        if let data = response {
            if let any = Utils.dataToAny(data: data) {
                success = true
                responseItem = any
            } else {
                success = false
                message = badServerResponse
            }
        } else {
            success = false
            message = badServerResponse
        }
    }
}

class RequestLayer: NSObject {

    typealias Success = (_ requestResponse: RequestResponse, _ httpResponse: URLResponse?) -> Void

    class func request(url: String!,
                       method: RequestMethod,
                       timeout: TimeInterval?,
                       body: JSON?,
                       completion: @escaping Success) {

        var timeoutUnwrap = 20.0

        if let timeoutU = timeout {
            timeoutUnwrap = Double(timeoutU)
        }

        guard let urlRequest = URL(string: url) else {
            var response = RequestResponse(withResponse: .none)
            response.message = generiErrorMessage
            completion(response, .none)
            return
        }

        var request = URLRequest(url: urlRequest,
                                 cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData,
                                 timeoutInterval: timeoutUnwrap)

        if method == .put || method == .post {
            if let bodyU = body {
                if let data = Utils.jsonToData(json: bodyU) {
                    request.httpBody = data
                } else {
                    var response = RequestResponse(withResponse: .none)
                    response.message = generiErrorMessage
                    completion(response, .none)
                    return
                }
            } else {
                var response = RequestResponse(withResponse: .none)
                response.message = generiErrorMessage
                completion(response, .none)
                return
            }
        }

        request.httpMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task: URLSessionDataTask = session.dataTask(with: request) { (data, urlResponse, error) -> Void in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(RequestResponse(withResponse: data), urlResponse)
            }
        }
        task.resume()
    }

}
