

import UIKit

protocol Displayable {
    associatedtype T
    var display: T? { get set }
    func configure(withDisplay display: T)
}
