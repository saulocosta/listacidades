//
//  Utils.swift
//  ListaCidades
//
//  Created by Saulo Costa on 29/07/17.
//  Copyright © 2017 Saulo. All rights reserved.
//

import UIKit

class Utils: NSObject {

    class func jsonToData(json: Any) -> Data? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            
            return jsonData
        } catch {
            print(error.localizedDescription)
            return .none
        }
    }

    class func dataToAny(data: Data) -> Any? {
        do {
            let decoded = try JSONSerialization.jsonObject(with: data,
                                                             options: JSONSerialization.ReadingOptions.allowFragments)
            return decoded
        } catch {
            print(error.localizedDescription)
            return .none
        }
    }
    
    class func cleanString(string: String) -> String {
        return string.folding(options: .diacriticInsensitive, locale: NSLocale.current).lowercased()
    }

    class func setSearchApplicationRootVC(withCityList list:[City]) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setSearchApplicationRootVC(withCityList: list)
    }

    class func stringFormaterFrom(number: NSNumber) -> String {
        let numberFormatter = NumberFormatter()
        
        numberFormatter.locale = Locale(identifier: "pt_BR")
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.minimumIntegerDigits = 1
        numberFormatter.numberStyle = .decimal
        
        if let numberString = numberFormatter.string(from: number) {
            return "\(numberString)"
        } else {
            return "0"
        }
    }
}
