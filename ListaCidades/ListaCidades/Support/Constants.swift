
import UIKit

let baseUrl = "http://wsteste.devedp.com.br/Master/CidadeServico.svc/rest/"

let defaultAnimationDuration: TimeInterval = 0.3

let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted)

let heatmapUpdateInterval = 16

extension NSNotification.Name {
    public static let logout: NSNotification.Name =
        NSNotification.Name(rawValue: "LOGOUT")
}

typealias Fail = (_ failMessage: String) -> Void

let generiErrorMessage = NSLocalizedString("Ocorreu um erro ao processar a requisição", comment: "")
let timeoutErrorMessage = NSLocalizedString("Sem conexão com a internet", comment: "")
let badServerResponse = NSLocalizedString("Ocorreu um erro em nosso servidor, tente novamente dentro de alguns instantes.", comment: "")
