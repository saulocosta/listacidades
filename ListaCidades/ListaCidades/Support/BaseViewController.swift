//
//  BaseViewController.swift
//  ListaCidades
//
//  Created by Saulo Costa on 29/07/17.
//  Copyright © 2017 Saulo. All rights reserved.
//

import UIKit
import SVProgressHUD

class BaseViewController: UIViewController {

    func showLoader() {
        SVProgressHUD.show()
    }
    
    func hideLoader() {
        SVProgressHUD.dismiss()
    }
    
    func showLoader(withStatus status: String) {
        SVProgressHUD.show(withStatus: status)
    }
    
    func showError(withStatus status: String) {
        SVProgressHUD.showError(withStatus: status)
    }
    
    func showSuccess(withStatus status: String) {
        SVProgressHUD.showSuccess(withStatus: status)
    }
    
    func showInfo(withStatus status: String) {
        SVProgressHUD.showInfo(withStatus: status)
    }
}
