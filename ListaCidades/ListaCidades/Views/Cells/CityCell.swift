//
//  CityCell.swift
//  ListaCidades
//
//  Created by Saulo Costa on 30/07/17.
//  Copyright © 2017 Saulo. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {
    
    // MARK: Outlets
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblState: UILabel!
    
    // MARK: - Properties
    static let identifier = "CityCell"
    var display: City?
}

extension CityCell: Displayable {
    func configure(withDisplay display: City) {

        guard let nome = display.nome,
        let estado = display.estado else { return }

        lblCity.text = "Cidade: \(nome)"
        lblState.text = "Estado: \(estado)"
    }
}
