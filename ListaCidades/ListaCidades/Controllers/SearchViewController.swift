//
//  SearchViewController.swift
//  ListaCidades
//
//  Created by Saulo Costa on 29/07/17.
//  Copyright © 2017 Saulo. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SearchViewController: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!

    // MARK: - Properties
    var cityList: [City]!
    var presenter: SearchVCPresenter?
    static let identifier = "SearchViewController"

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
        presenter = SearchVCPresenter(withDelegate: self, cityList: cityList)
    }

    // MARK: - Actions
    @IBAction func search() {
        if validate() {
            dismissKB()
            guard let cityName = txtCity.text,
                let stateName = txtState.text else { return }
            presenter?.filterList(withCityName: cityName, stateName: stateName)
        }
    }

    @IBAction func dismissKB() {
        txtCity.resignFirstResponder()
        txtState.resignFirstResponder()
    }

    // MARK: - Methods
    func setup() {
        txtCity.addDoneOnKeyboardWithTarget(self, action: #selector(search))
        txtState.addDoneOnKeyboardWithTarget(self, action: #selector(search))
    }
    
    func validate() -> Bool {
        guard let cityName = txtCity.text,
        let stateName = txtState.text else { return false }
        if cityName.isEmpty {
            showError(withStatus: NSLocalizedString("Preencha o nome da cidade", comment: ""))
            txtCity.becomeFirstResponder()
            return false
        }
        if stateName.isEmpty {
            showError(withStatus: NSLocalizedString("Preencha o nome do Estado", comment: ""))
            txtState.becomeFirstResponder()
            return false
        }
        return true
    }
}

// MARK: - Navigation
extension SearchViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SearchResultViewController {
            guard let cityList = sender as? [City] else { return }
            vc.cityList = cityList
        }
    }
}

// MARK: - UITextFieldDelegate
extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if textField == txtCity {
            txtState.becomeFirstResponder()
        } else if textField == txtCity {
            txtState.resignFirstResponder()
            search()
        }

        return true
    }
}

// MARK: - SearchVCPresenterDelegate
extension SearchViewController: SearchVCPresenterDelegate {

    func showSearchList(withItermList itemlist: [City]) {
        performSegue(withIdentifier: "SegueResult", sender: itemlist)
    }
}
