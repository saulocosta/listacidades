//
//  InitialVCPresenter.swift
//  ListaCidades
//
//  Created by Saulo Costa on 29/07/17.
//  Copyright © 2017 Saulo. All rights reserved.
//

import UIKit

protocol InitialVCPresenterDelegate: BaseProtocol {
    func setDownloadInfoVisibility(isVisible visible:Bool)
    func showRetryAlert()
}

class InitialVCPresenter: NSObject {
    
    // MARK: - Properties
    var delegate : InitialVCPresenterDelegate?
    
    // MARK: - Init
    init(withDelegate delegate: InitialVCPresenterDelegate) {
        self.delegate = delegate
        super.init()
        fetchCityList()
    }

    // MARK: - Methods
    func fetchCityList() {
        delegate?.setDownloadInfoVisibility(isVisible: true)
        APICity.getCityList(success: { (citylist) in
            Utils.setSearchApplicationRootVC(withCityList: citylist)
        }) { (_) in
            self.delegate?.setDownloadInfoVisibility(isVisible: false)
            self.delegate?.showRetryAlert()
        }
    }

}
