//
//  InitialViewController.swift
//  ListaCidades
//
//  Created by Saulo Costa on 29/07/17.
//  Copyright © 2017 Saulo. All rights reserved.
//

import UIKit

class InitialViewController: BaseViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var viewDownloadInfo: UIView!
    
    // MARK: - Properties
    var presenter: InitialVCPresenter?

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = InitialVCPresenter(withDelegate: self)
    }
}

// MARK: - InitialVCPresenterDelegate
extension InitialViewController: InitialVCPresenterDelegate {

    func setDownloadInfoVisibility(isVisible visible:Bool) {
        var alpha:CGFloat = 0.0
        if visible {
            alpha = 1.0
        }
        UIView.animate(withDuration: defaultAnimationDuration) {
            self.viewDownloadInfo.alpha = alpha
        }
    }
    
    func showRetryAlert() {
        let alertController = UIAlertController(title: NSLocalizedString("Atenção", comment: "") ,
                                                message: NSLocalizedString("Ocorreu um erro ao obtera a lista de cidades, por favor, tente novamente!", comment: ""),
                                                preferredStyle: .alert)
        
        let retry = UIAlertAction(title: NSLocalizedString("Tentar novamente", comment: ""), style: .default) { (action) in
            self.presenter?.fetchCityList()
        }
        alertController.addAction(retry)
        present(alertController, animated: true, completion: nil)
    }
}
