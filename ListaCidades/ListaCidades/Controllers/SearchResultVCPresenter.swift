//
//  SearchResultVCPresenter.swift
//  ListaCidades
//
//  Created by Saulo Costa on 30/07/17.
//  Copyright © 2017 Saulo. All rights reserved.
//

import UIKit

protocol SearchResultVCPresenterDelegate: BaseProtocol {
    func showCityPoints(_ points:NSNumber, city: City)
}

class SearchResultVCPresenter: NSObject {

    // MARK: - Properties
    var delegate: SearchResultVCPresenterDelegate?
    
    // MARK: - Init
    init(withDelegate delegate: SearchResultVCPresenterDelegate) {
        self.delegate = delegate
        super.init()
    }
    
    // MARK: - Methods
    func getPoints(fromCity city: City) {
        delegate?.showLoader()
        APICity.getCityPoints(city: city, success: { (point) in
            self.delegate?.hideLoader()
            self.delegate?.showCityPoints(point, city: city)
        }) { (failMessage) in
            self.delegate?.showError(withStatus: failMessage)
        }
    }
}
