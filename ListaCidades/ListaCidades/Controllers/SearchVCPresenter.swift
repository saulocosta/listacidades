//
//  SearchVCPresenter.swift
//  ListaCidades
//
//  Created by Saulo Costa on 29/07/17.
//  Copyright © 2017 Saulo. All rights reserved.
//

import UIKit

protocol SearchVCPresenterDelegate: BaseProtocol {
    func showSearchList(withItermList itemlist: [City])
}

class SearchVCPresenter: NSObject {

    // MARK: - Properties
    var delegate: SearchVCPresenterDelegate?
    var cityList: [City]
    
    // MARK: - Init
    init(withDelegate delegate: SearchVCPresenterDelegate, cityList: [City]) {
        self.delegate = delegate
        self.cityList = cityList
        super.init()
    }

    // MARK: - Methods
    func filterList(withCityName cityName:String, stateName:String) {

        let filteredList = APICity.filter(withList: cityList, cityName: cityName, stateName: stateName)

        if filteredList.isEmpty {
            delegate?.showError(withStatus: NSLocalizedString("Nenhum item encontrado com estes parâmetros de busca", comment: ""))
        } else {
            delegate?.showSearchList(withItermList: filteredList)
        }
    }
}
