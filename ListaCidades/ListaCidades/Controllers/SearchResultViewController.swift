//
//  SearchResultViewController.swift
//  ListaCidades
//
//  Created by Saulo Costa on 30/07/17.
//  Copyright © 2017 Saulo. All rights reserved.
//

import UIKit

class SearchResultViewController: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var cityList: [City]!
    var presenter: SearchResultVCPresenter?
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
        presenter = SearchResultVCPresenter(withDelegate: self)
    }

    // MARK: - Methods
    func setup() {
        tableView.register(UINib(nibName: "CityCell", bundle: nil), forCellReuseIdentifier: CityCell.identifier)
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
    }
}

// MARK: - UITableViewDataSource
extension SearchResultViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CityCell.identifier, for: indexPath)
        (cell as? CityCell)?.configure(withDisplay: cityList[indexPath.row])
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SearchResultViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.getPoints(fromCity: cityList[indexPath.row])
    }
}

// MARK: - SearchResultVCPresenterDelegate
extension SearchResultViewController: SearchResultVCPresenterDelegate {
    
    func showCityPoints(_ points:NSNumber, city: City) {

        guard let cityName = city.nome else { return }
        let cityPoints = Utils.stringFormaterFrom(number: points)

        let alertMessage = "A pontuação da Cidade de \(cityName) é \(cityPoints)"

        let alertController = UIAlertController(title: NSLocalizedString("Alert", comment: "") ,
                                                message: alertMessage,
                                                preferredStyle: .alert)
        
        let ok = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .cancel) { (action) in }
        
        alertController.addAction(ok)
        present(alertController, animated: true, completion: nil)
    }
}
