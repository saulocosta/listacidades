//
//  APICity.swift
//  ListaCidades
//
//  Created by Saulo Costa on 29/07/17.
//  Copyright © 2017 Saulo. All rights reserved.
//

import UIKit

private enum CityEndpoints: String {
    case BuscaTodasCidades = "BuscaTodasCidades"
    case BuscaPontos = "BuscaPontos"
}

class APICity: NSObject {

    //MARK: - BuscaTodasCidades
    typealias SuccessGetCityList = (_ cityList: [City]) -> Void
    
    class func getCityList(success: @escaping SuccessGetCityList,
                     fail: @escaping Fail) {

        let url = baseUrl+CityEndpoints.BuscaTodasCidades.rawValue
        
        RequestLayer.request(url: url, method: .get, timeout: .none, body: .none) { (response, urlResponse) in
            if response.success {

                guard let responseArray = response.responseItem as? [[String:Any]] else {
                    fail(response.message)
                    return
                }

                var itemList = [City]()
                for item in responseArray {
                    if let city = City(json: item) {
                        itemList.append(city)
                    }
                }

                success(itemList)
            } else {
                fail(response.message)
            }
        }
    }

    //MARK: - BuscaPontos
    typealias SuccessGetCityPoints = (_ cityPoints: NSNumber) -> Void

    class func getCityPoints(city: City,
                       success: @escaping SuccessGetCityPoints,
                       fail: @escaping Fail) {
        
        let url = baseUrl+CityEndpoints.BuscaPontos.rawValue
        
        RequestLayer.request(url: url, method: .post, timeout: .none, body: city.toJSON()) { (response, urlResponse) in
            if response.success {
                
                guard let responseItem = response.responseItem as? NSNumber else {
                    fail(response.message)
                    return
                }
                
                success(responseItem)
            } else {
                fail(response.message)
            }
        }
    }
    
    //MARK: - FilterList
    class func filter(withList list:[City], cityName:String, stateName:String) -> [City] {
        //Remove upercase and accent
        let cleanCityName = Utils.cleanString(string: cityName)
        let cleanStateName = Utils.cleanString(string: stateName)
        
        let filteredList = list.filter({
            
            //Again, we have to remove it in the itens in list
            let cleanCityNameItemList = Utils.cleanString(string: $0.nome)
            let cleanStateNameItemList = Utils.cleanString(string: $0.estado)
            
            if cleanCityNameItemList.contains(cleanCityName) && cleanStateNameItemList.contains(cleanStateName) {
                return true
            } else {
                return false
            }
        })
        return filteredList
    }
}
