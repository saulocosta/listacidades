//
//	City.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation
import Gloss

//MARK: - City
public struct City: Glossy {

	public var estado : String!
	public var nome : String!

	//MARK: Decodable
	public init?(json: JSON){
		estado = "Estado" <~~ json
		nome = "Nome" <~~ json
        if estado == nil {
            estado = ""
        }
        if nome == nil {
            nome = ""
        }
	}

	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
		"Estado" ~~> estado,
		"Nome" ~~> nome,
		])
	}
}
