//
//  ListaCidadesTests.swift
//  ListaCidadesTests
//
//  Created by Saulo Costa on 28/07/17.
//  Copyright © 2017 Saulo. All rights reserved.
//

import XCTest
@testable import ListaCidades

class ListaCidadesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testCityFetch() {
        APICity.getCityList(success: { (list) in
            if list.isEmpty {
                XCTAssert(false)
            } else {
                XCTAssert(true)
            }
            
        }) { (fail) in
            if fail.characters.isEmpty {
                XCTAssert(false)
            } else {
                XCTAssert(true)
            }
        }
    }
    
    func testCityPoints() {
        var city = City(json: ["Nome":"Barueri","Estado":"São Paulo"])
        city?.nome = "Barueri"
        city?.estado = "São Paulo"

        APICity.getCityPoints(city: city!, success: { (point) in
            XCTAssert(true)
        }) { (fail) in
            if fail.characters.isEmpty {
                XCTAssert(false)
            } else {
                XCTAssert(true)
            }
        }
    }
}
